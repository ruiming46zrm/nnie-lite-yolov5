import os

import cv2
from detectz import Detector
from utils.plots import Annotator, colors, save_one_box
from utils.torch_utils import select_device, time_sync
import numpy as np
from tqdm import tqdm

def check_prefix(img_p):
    img_prifixes = ['.jpg', '.png', '.jpeg']
    for img_prifix in img_prifixes:
        if img_p.endswith(img_prifix):
            return True
    return False


def find_img_folders(res_list, root_path):
    if os.path.isfile(root_path):
        return res_list
    for whatever_p in os.listdir(root_path):
        whatever_path = os.path.join(root_path, whatever_p)
        if os.path.isfile(whatever_path):
            if root_path not in res_list:
                res_list.append(root_path)
        else:
            res_list = find_img_folders(res_list, whatever_path)
    return res_list


class R:
    def __init__(self):
        self.show_flg = False
        self.model_path = './last.pt'
        self.detector          =  Detector(self.model_path, imgsz=416)
        print()

    def r(self):
        img = cv2.imread('1.jpg')
        img = cv2.resize(img, (416, 416))
        r = self.detector.detect(img)
        print(r)
        if self.show_flg:
            show_img = self.detector.make_show(img, r)
            cv2.imshow('res', show_img)
            cv2.waitKey(0)

def t():
    r = R()
    r.r()

if __name__ == '__main__':
    t()
