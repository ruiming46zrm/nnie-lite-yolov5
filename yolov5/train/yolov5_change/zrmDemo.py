import os

import cv2
import numpy as np

from detect import Detector
from utils.torch_utils import time_sync


def check_prefix(img_p):
    img_prifixes = ['.jpg', '.png', '.jpeg']
    for img_prifix in img_prifixes:
        if img_p.endswith(img_prifix):
            return True
    return False


def find_img_folders(res_list, root_path):
    if os.path.isfile(root_path):
        return res_list
    for whatever_p in os.listdir(root_path):
        whatever_path = os.path.join(root_path, whatever_p)
        if os.path.isfile(whatever_path):
            if root_path not in res_list:
                res_list.append(root_path)
        else:
            res_list = find_img_folders(res_list, whatever_path)
    return res_list


class Runner:
    def __init__(self):
        self.show_flg = True
        self.model_path =  './runs/train/deal_change/exp3/last4m.pt'
        self.detector          =  Detector(self.model_path, imgsz=608, letter_box=True)

    def test_time(self):
        time_list = []
        img = cv2.imread('./data/images/zidane.jpg')
        range_num = 1 if self.show_flg else 100
        for i in range(range_num):
            t0 = time_sync()
            r = self.detector.detect(img)
            t1 = time_sync()
            # print(t1 - t0)
            time_list.append(t1 - t0)
        avg_infer_time = np.mean(time_list)
        if self.show_flg:
            show_img = self.detector.make_show(img, r)
            cv2.imshow('res', show_img)
            cv2.waitKey(0)

        print('avg infer time (include pre+post process) : {}'.format(np.round(avg_infer_time, 4)))


    def infer_file(self, file_root, out_file_root):
        if not os.path.exists(out_file_root):
            os.makedirs(out_file_root)
        model_name = self.model_path.split('/')[-1].split('.')[0]
        for img_p in os.listdir(file_root):
            if not check_prefix(img_p):
                continue
            out_img_p = img_p.split('.')[0] + '_' + model_name + '.jpg'
            img_path = os.path.join(file_root, img_p)
            out_img_path = os.path.join(out_file_root, out_img_p)
            img = cv2.imread(img_path)
            img = cv2.resize(img, (416, 416))
            if img is None:
                continue
            r = self.detector.detect(img)
            print(r)
            show_img = self.detector.make_show(img, r)
            cv2.imwrite(out_img_path, show_img)

    # only for fp test data
    def infer_multi_file(self, multi_file_root):
        save_flg = 'deal12'
        key_name = multi_file_root.strip().split('/')[-1]
        img_folders = find_img_folders([], multi_file_root)
        out_folders = [i.replace(key_name, key_name + '_' + save_flg) for i in img_folders]
        for img_folder in img_folders:
            out_folder = img_folder.replace(key_name, key_name + '_' + save_flg)
            print('-> deal {} ->{}'.format(img_folder, len(os.listdir(img_folder))))
            self.infer_file(img_folder, out_folder)
        print()



def t():
    runner = Runner()

    multi_file_root = './imgs'
    multi_file_root_out = './imgs_out'
    runner.infer_file(multi_file_root, multi_file_root_out)

if __name__ == '__main__':
    t()
