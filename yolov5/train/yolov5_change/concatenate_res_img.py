import os

import cv2
import numpy as np
from tqdm import tqdm

from zrmDemo import check_prefix, find_img_folders


def name_match(img_p0):
    img_p1 = img_p0
    return img_p1

def concat_one_folder(folder0, folder1, folder_out):
    if not os.path.exists(folder_out):
        os.makedirs(folder_out)
    max_width, max_height = 1600, 800
    for img_p0 in os.listdir(folder0):
        if not check_prefix(img_p0):
            continue
        img_p1 = name_match(img_p0)
        img_path0 = os.path.join(folder0, img_p0)
        img_path1 = os.path.join(folder1, img_p1)
        img_path_out = os.path.join(folder_out, img_p0)

        img0 = cv2.imread(img_path0)
        img1 = cv2.imread(img_path1) if os.path.exists(img_path1) else np.zeros_like(img0)
        h, w = img0.shape[:2]
        resize_rate = min(max_height  * 0.5/ h, max_width * 0.5 /w)
        # img0 = cv2.resize(img0, (0, 0), fx=resize_rate, fy=resize_rate)
        # img1 = cv2.resize(img1, (0, 0), fx=resize_rate, fy=resize_rate)
        img0 = cv2.resize(img0, (800, 400))
        img1 = cv2.resize(img1, (800, 400))
        cv2.putText(img0, '3k process', (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        cv2.putText(img1, '3k process delete', (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        top_down = max_height / h > max_width / w
        if top_down:
            out_img = np.concatenate((img0, img1), axis=0)
        else:
            out_img = np.concatenate((img0, img1), axis=1)
        cv2.imwrite(img_path_out, out_img)


def concat_one_folder_video(folder0, folder1, folder_out, top_down=False):
    folder_p = folder0.split('/')[-2]
    if not os.path.exists(folder_out):
        os.makedirs(folder_out)
    frame_w, frame_h = 1600, 400
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    show_size = (int(frame_w), int(frame_h))
    save_path = os.path.join(folder_out, 'res_{}.mp4'.format(folder_p))
    save_fps = 5
    writer = cv2.VideoWriter(save_path, fourcc, save_fps, show_size)
    img_list = os.listdir(folder0)
    img_list = sorted(img_list, key=lambda this_p: this_p.split('_')[0])
    for img_p0 in img_list:
        if not check_prefix(img_p0):
            continue
        img_p1 = name_match(img_p0)
        img_path0 = os.path.join(folder0, img_p0)
        img_path1 = os.path.join(folder1, img_p1)
        img_path_out = os.path.join(folder_out, img_p0)

        img0 = cv2.imread(img_path0)
        img1 = cv2.imread(img_path1) if os.path.exists(img_path1) else np.zeros_like(img0)
        img0 = cv2.resize(img0, (800, 400))
        img1 = cv2.resize(img1, (800, 400))
        cv2.putText(img0, '3k process', (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        cv2.putText(img1, '4k process', (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        top_down = False
        if top_down:
            out_img = np.concatenate((img0, img1), axis=0)
        else:
            out_img = np.concatenate((img0, img1), axis=1)
        cv2.imwrite(img_path_out, out_img)
        writer.write(out_img)
    writer.release()

# only to adjust 3k test
def concat_multi_folder(multi_file_root, multi_file_compare_root):
    save_flg = 'compare_0517'
    key_name = multi_file_root.strip().split('/')[-1]
    key_name_compare = multi_file_compare_root.strip().split('/')[-1]
    img_folders = find_img_folders([], multi_file_root)
    for img_folder in tqdm(img_folders):
        compare_folder = img_folder.replace(key_name, key_name_compare)
        out_folder = img_folder.replace(key_name, save_flg)
        concat_one_folder_video(img_folder, compare_folder, out_folder)
        # concat_one_folder(img_folder, compare_folder, out_folder)



if __name__ == '__main__':
    # concat_one_folder(
    #     '/home/z/work/data/test/测试图片_origin/场景1-暗室杂物误检/hisi_img',
    #     '/home/z/work/data/test/测试图片_deal/场景1-暗室杂物误检/hisi_img',
    #     ''
    # )

    multi_file = '/home/z/work/data/test/测试图片_deal11'
    multi_file_compare = '/home/z/work/data/test/测试图片_deal12'
    concat_multi_folder(multi_file, multi_file_compare)
