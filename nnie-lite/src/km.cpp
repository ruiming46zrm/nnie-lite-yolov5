#include "algo_utils.h"

using namespace std;
const MS_S32 MAXN = 100;
const MS_S32 INF = 0x3f3f3f3f;

MS_S32 iou[MAXN][MAXN];     // 记录每个car和每个space的iou
MS_S32 ex_space[MAXN];      // 每个space的期望值
MS_S32 ex_car[MAXN];        // 每个car的期望值
bool vis_space[MAXN];    // 记录每一轮匹配匹配过的space
bool vis_car[MAXN];      // 记录每一轮匹配匹配过的car
MS_S32 match[MAXN];         // 记录每个car匹配到的space 如果没有则为-1
MS_S32 slack[MAXN];         // 记录每个car如果能被space倾心最少还需要多少期望值
MS_S32 space_mask[MAXN];    // 记录每个space对所有car的最高iou（期望值）太低时，不进行匹配，MASK置1


bool dfs(MS_S32 space, MS_S32 car_num)
{
    vis_space[space] = true;

    for (int car = 0; car < car_num; ++car) {

        if (vis_car[car]) continue; // 每一轮匹配 每个car只尝试一次

        int gap = ex_space[space] + ex_car[car] - iou[space][car];
        
        if (gap == 0) {  // 如果符合要求
            // printf("---\n");
            // printf("space:%d gap: %d\n", space, gap);
            // printf("   car:%d match: %d\n", car, match[car]);
            vis_car[car] = true;
            if (match[car] == -1 || dfs( match[car], car_num)) {    // 找到一个没有匹配的car 或者该car的space可以找到其他car
                match[car] = space;
                // printf("   %d -> %d , match[%d] -> %d\n", space, car, car, space);
                return true;
            }
        } else {
            slack[car] = min(slack[car], gap);  // slack 可以理解为该car要匹配上space, 还需多少期望值 取最小值 
        }
    }

    return false;
}



MS_S32 KM(MS_FLOAT* lv, MS_S32 car_num, MS_S32 space_num, MS_S32* car_match, MS_S32* space_match)
{
    // lv : car * space struct
    // printf("----givinig value-----\n");
    for (MS_S32 i=0; i < car_num; i++){
        car_match[i] = -1;
        for(MS_S32 j=0; j < space_num; j++){
            space_match[j] = -1;
            space_mask[j] = 0;
            MS_FLOAT float_value = lv[i * space_num + j];
            MS_S32 int_value = (MS_S32) (float_value * 100);
            iou[j][i] = int_value;
            // printf("%f -> %d  ", float_value, int_value);
        }
        // printf("\n");
    }

    // printf("--- love ----\n");
    // for(MS_S32 space_id=0; space_id < space_num; space_id++){
    //     for(MS_S32 car_id=0; car_id < car_num; car_id++){
    //         printf("%d  ", love[space_id][car_id]);
    //     }
    //     printf("\n");
    // }

    memset(match, -1, sizeof match);    // 初始每个car都没有匹配的space
    memset(ex_car, 0, sizeof ex_car);   // 初始每个car的期望值为0\
    // memset(car_match, -1, sizeof car_match);
    // memset(space_match, -1, sizeof space_match);
    // memset(space_mask, 0, sizeof space_mask); 
    

    // 每个space的初始期望值是与她相连的男生最大的好感度
    for (MS_S32 i = 0; i < space_num; ++i) {
        ex_space[i] = iou[i][0];
        for (MS_S32 j = 1; j < car_num; ++j) {
            ex_space[i] = max(ex_space[i], iou[i][j]);
        }
        // printf("space:%d, exp:%d\n", i, ex_space[i]);
        if(ex_space[i] < 0.05){
            space_mask[i] = 1;
        }
    }

    // 尝试为每一个space解决归宿问题
    for (MS_S32 i = 0; i < space_num; ++i) {
        if(space_mask[i] == 1){
            continue;;
        }
        fill(slack, slack + car_num, INF);    // 因为要取最小值 初始化为无穷大
        while (1) {
            // 为每个space解决归宿问题的方法是 ：如果找不到就降低期望值，直到找到为止

            // 记录每轮匹配中car space是否被尝试匹配过
            memset(vis_space, false, sizeof vis_space);
            memset(vis_car, false, sizeof vis_car);

            if (dfs(i, car_num)) break;  // 找到 退出

            // 如果不能找到 就降低期望值
            // 最小可降低的期望值
            MS_S32 d = INF;
            for (MS_S32 j = 0; j < car_num; ++j)
                if (!vis_car[j]) d = min(d, slack[j]);

            for (MS_S32 j = 0; j < space_num; ++j) {
                // 所有访问过的space降低期望值
                if (vis_space[j]) ex_space[j] -= d;
            }

            for (MS_S32 j = 0; j < car_num; ++j) {
                // 所有访问过的男生增加期望值
                if (vis_car[j]) ex_car[j] += d;
                // 没有访问过的car 因为space们的期望值降低，距离又进了一步
                else slack[j] -= d;
            }
        }
    }

    // 匹配完成 求出所有配对的好感度的和
    MS_S32 res = 0;
    for (MS_S32 i = 0; i < car_num; ++i){
        car_match[i] = match[i];
        space_match[match[i]] = i;
        if(match[i] >= 0){
            MS_S32 addItem = iou[match[i]][i];
            res += addItem;
            // printf("add %d , res is %d\n", addItem, res);
        }
    }
    return res;
}