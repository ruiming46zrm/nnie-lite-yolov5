#include "parking.h"
#include <stdio.h>
#include <vector>
#include <deque>
#include <chrono>
#include <exception> 
#include "algo_utils.h"

MS_PARKING_CFG_S* parkingCfg;
MS_FLOAT detectThresh;
MS_FLOAT occupancyThresh;
std::vector<std::deque<MS_FLOAT>> judgeParkingQueue;
NNIE_Obj_S* mAlarmObj = new NNIE_Obj_S[OCCUPANCY_SENS_MAX];
PARKING_SPACE_S* mAlarmSpace= new PARKING_SPACE_S[OCCUPANCY_SENS_MAX];
struct timeval tv;

MS_VOID printMultiIou(MS_FLOAT* iouRes, MS_S32 num, NNIE_Obj_S* objList){
    if(!PRINT_DEBUG) return;
    printf("\n--print iou:\n");
    for(int i = 0; i < num; i++){
        printf("car: %2d  ->  [x:%3d,y:%3d,w:%3d,h:%3d]  ->  ", i, objList[i].u16X, objList[i].u16Y, objList[i].u16W, objList[i].u16H);
        for (int j = 0; j < parkingCfg->spaceNum; j++){
            MS_FLOAT res = iouRes[i * parkingCfg->spaceNum + j];
            printf("%3.3f   ", abs(res));
        }
        printf("\n");
    }
}

MS_VOID print_match(MS_S32* carMatch, MS_S32* space_match, MS_S32 carNum){
    if(!PRINT_DEBUG) return;
    printf("\n--print match res:\n");
    for(MS_S32 carId=0; carId < carNum; carId++){
        MS_S32 matchedSpaceId = carMatch[carId];
        printf("car:%2d -> %2d\n", carId, matchedSpaceId);
    }
    printf("\n");
    for(MS_S32 spaceId=0; spaceId < parkingCfg->spaceNum; spaceId++){
        MS_S32 matchedCarId = space_match[spaceId];
        printf("space:%2d -> %2d\n", spaceId, matchedCarId);
    }
}

MS_VOID printRes(MS_PARKING_RESULT* result){
    if(!PRINT_DEBUG) return;
    printf("\n--print  seq  res:\n");
    for(MS_S32 n=0; n<judgeParkingQueue.size(); n++){
        MS_FLOAT resFloat = 0;
        MS_S32 resBool = 0;
        printf("space_id:%d -> [", n);
        for (MS_S32 m=0; m<PARKING_JUDGE_FRAME_MAX; m++){
            resFloat += judgeParkingQueue[n][m];
            printf("%3.2f ", judgeParkingQueue[n][m]);
        }
        resFloat /= PARKING_JUDGE_FRAME_MAX;
        resFloat > occupancyThresh ? resBool = 1: resBool = 0;
        printf("] -> %3.2f/%3.2f -> %d\n", resFloat, occupancyThresh, resBool);
    }


    printf("\n--print final res:\n");
    for(MS_S32 n=0; n<result->objNum; n++){
        printf("%d/%d  car:[x:%3d,y:%3d,w:%3d,h:%3d] -> space:[%2d: (%3d,%3d)(%3d,%3d)(%3d,%3d)(%3d,%3d)]\n",n,result->objNum, 
            result->alarmObj[n].u16X, result->alarmObj[n].u16Y, result->alarmObj[n].u16W, result->alarmObj[n].u16H, 
            result->alarmSpace[n].id,
            result->alarmSpace[n].location.vertexs[0].s32X, result->alarmSpace[n].location.vertexs[0].s32Y, 
            result->alarmSpace[n].location.vertexs[1].s32X, result->alarmSpace[n].location.vertexs[1].s32Y, 
            result->alarmSpace[n].location.vertexs[2].s32X, result->alarmSpace[n].location.vertexs[2].s32Y, 
            result->alarmSpace[n].location.vertexs[3].s32X, result->alarmSpace[n].location.vertexs[3].s32Y
        );
    };
}



MS_VOID initParkingCfg(){
    // only for debug
    parkingCfg->detectSens = 1;
    parkingCfg->occupancySens = 4;
    parkingCfg->spaceNum = 4;
    parkingCfg->parkingSpaces[0].id = 0;

    parkingCfg->parkingSpaces[0].location.vertexs[0].s32X=0;
    parkingCfg->parkingSpaces[0].location.vertexs[0].s32Y=0;

    parkingCfg->parkingSpaces[0].location.vertexs[1].s32X=20;
    parkingCfg->parkingSpaces[0].location.vertexs[1].s32Y=0;

    parkingCfg->parkingSpaces[0].location.vertexs[2].s32X=20;
    parkingCfg->parkingSpaces[0].location.vertexs[2].s32Y=20;

    parkingCfg->parkingSpaces[0].location.vertexs[3].s32X=0;
    parkingCfg->parkingSpaces[0].location.vertexs[3].s32Y=20;




    parkingCfg->parkingSpaces[1].location.vertexs[0].s32X=336;
    parkingCfg->parkingSpaces[1].location.vertexs[0].s32Y=98;

    parkingCfg->parkingSpaces[1].location.vertexs[1].s32X=357;
    parkingCfg->parkingSpaces[1].location.vertexs[1].s32Y=98;

    parkingCfg->parkingSpaces[1].location.vertexs[2].s32X=368;
    parkingCfg->parkingSpaces[1].location.vertexs[2].s32Y=109;

    parkingCfg->parkingSpaces[1].location.vertexs[3].s32X=356;
    parkingCfg->parkingSpaces[1].location.vertexs[3].s32Y=113;





    parkingCfg->parkingSpaces[2].location.vertexs[0].s32X=147;
    parkingCfg->parkingSpaces[2].location.vertexs[0].s32Y=155;

    parkingCfg->parkingSpaces[2].location.vertexs[1].s32X=188;
    parkingCfg->parkingSpaces[2].location.vertexs[1].s32Y=155;

    parkingCfg->parkingSpaces[2].location.vertexs[2].s32X=188;
    parkingCfg->parkingSpaces[2].location.vertexs[2].s32Y=186;

    parkingCfg->parkingSpaces[2].location.vertexs[3].s32X=147;
    parkingCfg->parkingSpaces[2].location.vertexs[3].s32Y=186;





    parkingCfg->parkingSpaces[3].location.vertexs[0].s32X=359;
    parkingCfg->parkingSpaces[3].location.vertexs[0].s32Y=153;

    parkingCfg->parkingSpaces[3].location.vertexs[1].s32X=425;
    parkingCfg->parkingSpaces[3].location.vertexs[1].s32Y=152;

    parkingCfg->parkingSpaces[3].location.vertexs[2].s32X=462;
    parkingCfg->parkingSpaces[3].location.vertexs[2].s32Y=174;

    parkingCfg->parkingSpaces[3].location.vertexs[3].s32X=378;
    parkingCfg->parkingSpaces[3].location.vertexs[3].s32Y=177;

    for (int idx=0; idx<parkingCfg->spaceNum; idx++){
        parkingCfg->parkingSpaces[idx].id = idx;
        parkingCfg->parkingSpaces[idx].occupancy = MS_FALSE;
    }
}

MS_VOID initIouRes(MS_FLOAT* iouRes, MS_U32 rowNum, MS_U32 colNum){
    for(int i=0; i<rowNum; i++){
        for(int j=0; j<colNum; j++){
            iouRes[i*colNum + j] = 0;
        }
    }
};

MS_VOID initPerFrame(MS_S32 *carMatch, MS_S32 carNum, MS_S32 *spaceMatch, MS_S32 spaceNum){
    for(MS_S32 carIdx=0; carIdx<carNum; carIdx++){
        carMatch[carIdx] = -1;
    }
    for(MS_S32 spaceIdx=0; spaceIdx<spaceNum; spaceIdx++){
        spaceMatch[spaceIdx] = -1;
    }
}

MS_BOOL MS_Parking_Init(MS_PARKING_CFG_S* cfg){
    if (cfg->occupancySens>10||cfg->occupancySens<1||cfg->detectSens>10||cfg->detectSens<1){
        printf("false init\n");
        return MS_FALSE;
    }
    parkingCfg = cfg;
    initParkingCfg(); // only for debug
    MS_FLOAT detectTHreshArr[] = {0.35, 0.3, 0.25, 0.20, 0.15, 0.14, 0.13, 0.12, 0.11, 0.1};
    detectThresh = detectTHreshArr[parkingCfg->detectSens - 1];
    occupancyThresh = 1.0 - (MS_FLOAT)(parkingCfg->occupancySens-1) / (MS_FLOAT)OCCUPANCY_SENS_MAX;
    for(int placeId=0; placeId<parkingCfg->spaceNum; placeId++){
    std::deque<MS_FLOAT> placeJudgeQueue;
    for(int judgeIdx=0; judgeIdx<PARKING_JUDGE_FRAME_MAX; judgeIdx++){
        placeJudgeQueue.push_back(0.0);
    }
    judgeParkingQueue.push_back(placeJudgeQueue);
    }
    // return MS_TRUE;
}

MS_VOID MS_Parking_DeInit(MS_VOID* handdle){
    judgeParkingQueue.clear();
    delete[] mAlarmObj;
    delete[] mAlarmSpace;
}



MS_FLOAT calIou(MS_FLOAT* objPoint, MS_FLOAT* spacePoint){
    MS_FLOAT res = 0.8;

    MS_S32 op_x1 = (MS_DOUBLE)objPoint[0];
    MS_S32 op_y1 = (MS_DOUBLE)objPoint[1];
    MS_S32 op_x2 = (MS_DOUBLE)objPoint[2];
    MS_S32 op_y2 = (MS_DOUBLE)objPoint[3];


    MS_S32 sp_x1 = (MS_DOUBLE)spacePoint[0];
    MS_S32 sp_y1 = (MS_DOUBLE)spacePoint[1];
    MS_S32 sp_x2 = (MS_DOUBLE)spacePoint[2];
    MS_S32 sp_y2 = (MS_DOUBLE)spacePoint[3];
    MS_S32 sp_x3 = (MS_DOUBLE)spacePoint[4];
    MS_S32 sp_y3 = (MS_DOUBLE)spacePoint[5];
    MS_S32 sp_x4 = (MS_DOUBLE)spacePoint[6];
    MS_S32 sp_y4 = (MS_DOUBLE)spacePoint[7];

    // double p[8] = {0, 0, 1.5, 0, 1, 1, 0, 1};
    // double q[8] = {0.5, 0.5, 1.5, 0.5, 1.5, 1.5, 0.5, 1.5};
    // vector<double> P(p, p + 8);
    // vector<double> Q(q, q + 8);
    // iou_poly(P, Q);

    // double p[8] = {0, 0, 1, 0, 1, 1, 0, 1};
    // double q[8] = {0.5, 0.5, 1.5, 0.5, 1.5, 1.5, 0.5, 1.5};

    MS_DOUBLE p[8] = {op_x1, op_y1, op_x2, op_y1, op_x2, op_y2, op_x1, op_y2};
    MS_DOUBLE q[8] = {sp_x1, sp_y1, sp_x2, sp_y2, sp_x3, sp_y3, sp_x4, sp_y4};

    res = (MS_FLOAT)iou_poly(p, q);

    return res;
}

MS_VOID calMultiIou(NNIE_Obj_S* objList, MS_S32 boxNum, MS_FLOAT* iouRes, MS_FLOAT iouThresh){
    for(int boxIdx=0; boxIdx<boxNum; boxIdx++){
        for (int spaceIdx=0; spaceIdx<parkingCfg->spaceNum; spaceIdx++){
            // MS_S32 spaceId = parkingCfg->parkingSpaces[spaceIdx].id;
            MS_FLOAT objPoint[2][2] = {
                {objList[boxIdx].u16X, objList[boxIdx].u16Y},
                {objList[boxIdx].u16X + objList[boxIdx].u16W, objList[boxIdx].u16Y + objList[boxIdx].u16H}
            };
            MS_FLOAT spacePoint[4][2]{
                {parkingCfg->parkingSpaces[spaceIdx].location.vertexs[0].s32X, parkingCfg->parkingSpaces[spaceIdx].location.vertexs[0].s32Y},
                {parkingCfg->parkingSpaces[spaceIdx].location.vertexs[1].s32X, parkingCfg->parkingSpaces[spaceIdx].location.vertexs[1].s32Y},
                {parkingCfg->parkingSpaces[spaceIdx].location.vertexs[2].s32X, parkingCfg->parkingSpaces[spaceIdx].location.vertexs[2].s32Y},
                {parkingCfg->parkingSpaces[spaceIdx].location.vertexs[3].s32X, parkingCfg->parkingSpaces[spaceIdx].location.vertexs[3].s32Y},
            };
            MS_FLOAT thisIouRes = calIou(objPoint[0], spacePoint[0]);
            thisIouRes > iouThresh ? thisIouRes=thisIouRes: thisIouRes=0;
            iouRes[boxIdx * parkingCfg->spaceNum + spaceIdx] = thisIouRes;
        }
    }
}

MS_VOID MS_Parking_Get_Per_Frame(NNIE_Obj_S* objList, MS_S32 num, MS_S32 *carMatch, MS_S32 *spaceMatch){
    // for(MS_S32 i=0; i<num; i++){
    //     printf("%d %d %d %d %f\n",objList[i].u16X, objList[i].u16Y, objList[i].u16W, objList[i].u16H, objList[i].fClasScore);
    // }
    initPerFrame(carMatch, num, spaceMatch, parkingCfg->spaceNum);
    MS_FLOAT iouRes[num][parkingCfg->spaceNum];

    initIouRes(iouRes[0], num, parkingCfg->spaceNum);
    calMultiIou(objList, num, iouRes[0], 0.2);
    printMultiIou(iouRes[0], num, objList);
    MS_U32 res = KM(iouRes[0], num, parkingCfg->spaceNum, carMatch, spaceMatch);
    print_match(carMatch, spaceMatch, num);
}

MS_VOID MS_Sequential_Res_Get(NNIE_Obj_S* objList, MS_S32* carMatch, MS_S32* spaceMatch, MS_PARKING_RESULT* result){
    result->objNum = 0;
    result->SpaceNum = 0;
    
    for(int spaceIdx=0; spaceIdx<parkingCfg->spaceNum; spaceIdx++){
        MS_FLOAT seqRes = 0.0;
        if (spaceMatch[spaceIdx] >= 0){
            judgeParkingQueue[spaceIdx].push_back(1.0);
        } else{
            judgeParkingQueue[spaceIdx].push_back(0.0);
        }
        judgeParkingQueue[spaceIdx].pop_front();
        for (int seqIdx=0; seqIdx<PARKING_JUDGE_FRAME_MAX; seqIdx++){
            seqRes += judgeParkingQueue[spaceIdx][seqIdx];
        }
        seqRes /= PARKING_JUDGE_FRAME_MAX;
        auto now   = std::chrono::system_clock::now();
        auto nowSecond = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
        MS_U32 timeSecond = (MS_U32)nowSecond.count();

        if (seqRes > occupancyThresh){
            if(parkingCfg->parkingSpaces[spaceIdx].occupancy == MS_FALSE){
                parkingCfg->parkingSpaces[spaceIdx].startTime = timeSecond;
                parkingCfg->parkingSpaces[spaceIdx].occupancy = MS_TRUE;
            }
            // mAlarmObj[result->objNum] = objList[space_match[spaceIdx]];
            // mAlarmSpace[result->SpaceNum] = parkingCfg->parkingSpaces[spaceIdx];
            memcpy(&mAlarmObj[result->objNum], &objList[spaceMatch[spaceIdx]], sizeof(objList[spaceMatch[spaceIdx]]));
            memcpy(&mAlarmSpace[result->SpaceNum], &parkingCfg->parkingSpaces[spaceIdx], sizeof(parkingCfg->parkingSpaces[spaceIdx]));
            result->objNum++;
            result->SpaceNum++;
        } else{
            if(parkingCfg->parkingSpaces[spaceIdx].occupancy == MS_TRUE){
                parkingCfg->parkingSpaces[spaceIdx].endTime = timeSecond;
                parkingCfg->parkingSpaces[spaceIdx].occupancy = MS_FALSE;
            }
        }
        result->alarmObj = &(mAlarmObj[0]);
        result->alarmSpace = &(mAlarmSpace[0]);
    }
}

MS_VOID MS_Parking_Get(MS_VOID* handdle, NNIE_Obj_S* objList, MS_S32 num, MS_PARKING_RESULT* result){
    MS_S32 space_match[parkingCfg->spaceNum];
    MS_S32 car_match[num];
    MS_Parking_Get_Per_Frame(objList, num, car_match, space_match);
    MS_Sequential_Res_Get(objList, car_match, space_match, result);
    printRes(result);
}; 