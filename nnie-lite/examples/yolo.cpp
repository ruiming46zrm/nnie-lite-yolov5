#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <vector>
#include <sys/ioctl.h>
#include <dirent.h>
#include "Net.hpp"
#include "util.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/core.hpp"
#include "opencv2/dnn.hpp"
#include <fstream>
#include <sstream>
#include "parking.h"

/******************************************************************************
* function : show usage
******************************************************************************/

void getAllFiles(const char *path, std::vector<std::string> &files);

void Mat2bgr(cv::Mat im, unsigned char *data);

inline bool exists_test0 (const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}


void Usage(char *pchPrgName)
{
    printf("Usage : %s <index> \n", pchPrgName);
    printf("index:\n");
    printf("\t 1) Yolov3(Read File).\n");
    printf("\t 2) Yolov2(Read File).\n");
}

void yolov5ZrmDemo(const char *model_path, std::vector<std::string> &file_names){
    int doLetterBox = 1;
    nnie::Net yolov5_mnas;
    yolov5_mnas.load_model(model_path);
    printf("yolov5 start\n");
    std::vector<cv::Scalar> colorMap = {
        {0, 0, 255}, {0, 255, 0}, {255, 0, 0}, {71, 130, 255}, {238, 130, 238}, 
    };

    float r;
    float dw;
    float dh;

    for (std::string img_path: file_names){
        std::string out_img_path = img_path;
        std::string input_s = "input";
        std::string output_s = "output";
        int mannual_h = 544;
        int mannual_w = 960;

        out_img_path.replace(out_img_path.find(input_s), input_s.size(), output_s);
        if(exists_test0(out_img_path)){
            printf("already exist %s\n", out_img_path.c_str());
            continue;
        }

        printf("gen %s . . .\n", out_img_path.c_str());
        cv::Mat orignImg = cv::imread(img_path, cv::IMREAD_COLOR);
        cv::Mat resizedImg;
        cv::Mat dealImg;
        cv::Mat showImg;
        if (doLetterBox){
            printf("lets do letter box\n");
            LetterBox(orignImg, resizedImg, mannual_w, mannual_h, r, dw, dh);
            orignImg.copyTo(showImg);
            // cv::imwrite("letterbox.jpg", resizedImg);
        } else{
            cv::resize(orignImg, resizedImg, cv::Size(mannual_w, mannual_h));
            resizedImg.copyTo(showImg);
        }
        cv::cvtColor(resizedImg, dealImg, cv::COLOR_RGB2BGR);
        unsigned char *data = (unsigned char *) malloc(sizeof(unsigned char) * mannual_w * mannual_h * 3);
        Mat2bgr(dealImg, data);
        yolov5_mnas.run(data);

        nnie::Mat output0 = yolov5_mnas.getOutputTensor(0);    // output 2 !!!!!!!!
        nnie::Mat output1 = yolov5_mnas.getOutputTensor(1);
        nnie::Mat output2 = yolov5_mnas.getOutputTensor(2);    // output 0 !!!!!!!!

        int img_width = dealImg.cols;
        int img_height = dealImg.rows;
        int num_classes = 5;
        int kBoxPerCell = 3;

        int feature_index0 = 0;
        int feature_index1 = 1;
        int feature_index2 = 2;

        float conf_threshold = 0.15;
        float nms_threshold = 0.45;
        int is_nms = 1;
        int print_level = 1;

        std::vector<int> ids;
        std::vector<cv::Rect> boxes;
        std::vector<float> confidences;

        const std::vector<std::vector<cv::Size2f>> anchors ={
            {{116.0f, 90.0f}, {156.0f, 198.0f}, {373.0f, 326.0f}},
            {{30.0f, 61.0f}, {62.0f, 45.0f}, {59.0f, 119.0f}},
            {{10.0f, 13.0f}, {16.0f, 30.0f}, {33.0f, 23.0f}}};

        // printf("img_h:%d  img_w:%d\n", img_height, img_width);
        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index0,conf_threshold,
                        anchors[0],output0,ids,boxes,confidences, print_level);

        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index1,conf_threshold,
                        anchors[1],output1,ids,boxes,confidences, print_level);

        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index2,conf_threshold,
                        anchors[2],output2,ids,boxes,confidences, print_level);

        std::vector<int> indices;

        char * cls_names[] = {"bkground", "person", "car", "motorcycle", "bus", "truck"};
        std::vector<nnie::objInfo> detection_results;

        if (is_nms)
        {
            cv::dnn::NMSBoxes(boxes, confidences, conf_threshold, nms_threshold, indices);
        } else
        {
            for (int i = 0; i < boxes.size(); ++i)
            {
                indices.push_back(i);
            }
        }

        for (size_t i = 0; i < indices.size(); ++i){
            int idx = indices[i];
            cv::Rect box = boxes[idx];

            // remap box in src input size.
            cv::Rect2d remap_box;
            if (doLetterBox){
                remap_box = RemapBoxOnSrcLetterBox(cv::Rect2d(box), orignImg.cols, orignImg.rows, r, dw, dh);
            } else{
                remap_box = RemapBoxOnSrc(cv::Rect2d(box), img_width, img_height);
            }
            nnie::objInfo object_detection;
            object_detection.box = remap_box;
            object_detection.cls_id = ids[idx] + 1;
            object_detection.confidence = confidences[idx];
            detection_results.push_back(std::move(object_detection));
            float xmin = object_detection.box.x;
            float ymin = object_detection.box.y;
            float xmax = object_detection.box.x + object_detection.box.width;
            float ymax = object_detection.box.y + object_detection.box.height;
            float confidence = object_detection.confidence;
            int cls_id = object_detection.cls_id;
            char *cls_name = cls_names[cls_id];
            cv::Point p1(xmin, ymin);
            cv::Point p2(xmax, ymax);
            cv::Scalar color(0, 0, 255);
            cv::rectangle(showImg, p1, p2, colorMap[cls_id], 1);
            std::stringstream ss;
            ss<<confidence;
            std::string showText =  ss.str();
            showText = showText.substr(0, 4);
            float txt_scale = 0.73;
            cv::putText(showImg, showText, p1, cv::FONT_HERSHEY_PLAIN, txt_scale, colorMap[cls_id]);
            if(print_level>=0){
                printf("%d %s %.3f %.3f %.3f %.3f %.3f\n", cls_id, cls_name, confidence, xmin, ymin, xmax, ymax);
            }
        }
        cv::imwrite(out_img_path, showImg);
    }
    printf("yolov5 finish\n");
}


void yolov5ZrmDemoGenTxt(nnie::Net &yolov5_mnas, std::string image_dir){
    printf("yolov5 gen txt start\n");
    std::string input_s  = "tst_root";
    std::string output_s = "tst_resm";
    std::vector<std::string> ends = {
        ".jpg", ".png", ".jpeg", ".JPG", ".JPEG", ".PNG"
    };
    int mannual_h = 352;
    int mannual_w = 608;

    std::vector<std::string> file_names;
    getAllFiles(image_dir.c_str(), file_names);
    std::vector<cv::Scalar> colorMap = {
        {0, 0, 255}, {0, 255, 0}, {255, 0, 0}, {71, 130, 255}, {238, 130, 238}, 
    };
    std::string image_out_dir = image_dir;
    image_out_dir.replace(image_out_dir.find(input_s), input_s.size(), output_s);
    if(!exists_test0(image_out_dir)){
        std::string command;
        command = "mkdir -p " + image_out_dir;
        system(command.c_str());
    }
    int deal_index = 0;
    int total_len = file_names.size();
    float r;
    float dw;
    float dh;
    for (std::string img_path: file_names){
        deal_index++;
        std::string out_img_path = img_path;
        out_img_path.replace(out_img_path.find(input_s), input_s.size(), output_s);
        std::string out_txt_path = out_img_path;
        for (auto end: ends){
            int index = out_img_path.find(end);
            if(index >= 0){
                out_txt_path.replace(out_img_path.find(end), end.size(), ".txt");
            }
        }
        
        if(exists_test0(out_txt_path)){
            // printf("already exist %s\n", out_txt_path.c_str());
            continue;
        }

        printf("-> %d/%d: gen %s ...\n",deal_index, total_len, out_txt_path.c_str());
        FILE *fp=fopen(out_txt_path.c_str(), "wb");
        cv::Mat orignImg = cv::imread(img_path, cv::IMREAD_COLOR);
        cv::Mat resizedImg;
        cv::Mat dealImg;

        // cv::resize(orignImg, resizedImg, cv::Size(mannual_w, mannual_h));
        LetterBox(orignImg, resizedImg, mannual_w, mannual_h, r, dw, dh);

        cv::cvtColor(resizedImg, dealImg, cv::COLOR_RGB2BGR);
        unsigned char *data = (unsigned char *) malloc(sizeof(unsigned char) * mannual_w * mannual_h * 3);
        Mat2bgr(dealImg, data);
        yolov5_mnas.run(data);

        nnie::Mat output0 = yolov5_mnas.getOutputTensor(0);    // output 2 !!!!!!!!
        nnie::Mat output1 = yolov5_mnas.getOutputTensor(1);
        nnie::Mat output2 = yolov5_mnas.getOutputTensor(2);    // output 0 !!!!!!!!

        int img_width = dealImg.cols;
        int img_height = dealImg.rows;
        int num_classes = 5;
        int kBoxPerCell = 3;

        int feature_index0 = 0;
        int feature_index1 = 1;
        int feature_index2 = 2;

        float conf_threshold = 0.15;
        float nms_threshold = 0.45;
        int is_nms = 1;
        int print_level = 0;

        std::vector<int> ids;
        std::vector<cv::Rect> boxes;
        std::vector<float> confidences;

        const std::vector<std::vector<cv::Size2f>> anchors ={
            {{116.0f, 90.0f}, {156.0f, 198.0f}, {373.0f, 326.0f}},
            {{30.0f, 61.0f}, {62.0f, 45.0f}, {59.0f, 119.0f}},
            {{10.0f, 13.0f}, {16.0f, 30.0f}, {33.0f, 23.0f}}};

        // printf("img_h:%d  img_w:%d\n", img_height, img_width);
        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index0,conf_threshold,
                        anchors[0],output0,ids,boxes,confidences, print_level);

        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index1,conf_threshold,
                        anchors[1],output1,ids,boxes,confidences, print_level);

        parseYolov5Feature(img_width,img_height,num_classes,kBoxPerCell,feature_index2,conf_threshold,
                        anchors[2],output2,ids,boxes,confidences, print_level);

        std::vector<int> indices;

        char * cls_names[] = {"bkground", "person", "car", "motorcycle", "bus", "truck"};
        std::vector<nnie::objInfo> detection_results;

        if (is_nms)
        {
            cv::dnn::NMSBoxes(boxes, confidences, conf_threshold, nms_threshold, indices);
        } else
        {
            for (int i = 0; i < boxes.size(); ++i)
            {
                indices.push_back(i);
            }
        }

        for (size_t i = 0; i < indices.size(); ++i){
            int idx = indices[i];
            cv::Rect box = boxes[idx];

            // remap box in src input size.
            // auto remap_box = RemapBoxOnSrc(cv::Rect2d(box), img_width, img_height);
            auto remap_box = RemapBoxOnSrcLetterBox(cv::Rect2d(box), orignImg.cols, orignImg.rows, r, dw, dh);
            nnie::objInfo object_detection;
            object_detection.box = remap_box;
            object_detection.cls_id = ids[idx] + 1;
            object_detection.confidence = confidences[idx];
            detection_results.push_back(std::move(object_detection));
            float xmin = object_detection.box.x;
            float ymin = object_detection.box.y;
            float xmax = object_detection.box.x + object_detection.box.width;
            float ymax = object_detection.box.y + object_detection.box.height;
            float confidence = object_detection.confidence;
            int cls_id = object_detection.cls_id;
            char *cls_name = cls_names[cls_id];
            float xc = (xmin + xmax)/2;
            float yc= (ymin + ymax)/2;
            // cv::Point p1(xmin, ymin);
            // cv::Point p2(xmax, ymax);
            // cv::Scalar color(0, 0, 255);
            // cv::rectangle(resizedImg, p1, p2, colorMap[cls_id], 1);
            // std::stringstream ss;
            // ss<<confidence;
            // std::string showText =  ss.str();
            // showText = showText.substr(0, 4);
            // float txt_scale = 0.73;
            // cv::putText(resizedImg, showText, p1, cv::FONT_HERSHEY_PLAIN, txt_scale, colorMap[cls_id]);

            // if(print_level>=0){
            //     printf("%d/%d -> %d %s %.3f %.3f %.3f %.3f %.3f\n",deal_index, total_len, cls_id, cls_name, confidence, xmin, ymin, xmax, ymax);
            // }
            fprintf(fp,"%d %.6f %.6f %.6f %.6f %.6f\n", cls_id - 1, confidence, xc/orignImg.cols, yc/orignImg.rows, 
                object_detection.box.width/orignImg.cols, object_detection.box.height/orignImg.rows);
        }
        // cv::imwrite(out_img_path, resizedImg);
        fclose(fp);
    }
    printf("yolov5 finish\n");
}



void v5Infer(nnie::Net &yolov5_mnas, cv::Mat &orignImg, std::vector<nnie::objInfo> &detection_results){
    cv::Mat resizedImg;
    cv::Mat dealImg;
    cv::resize(orignImg, resizedImg, cv::Size(608, 352));
    cv::cvtColor(resizedImg, dealImg, cv::COLOR_RGB2BGR);
    unsigned char *data = (unsigned char *) malloc(sizeof(unsigned char) * 608 * 352 * 3);
    Mat2bgr(dealImg, data);
    yolov5_mnas.run(data);

    nnie::Mat output0 = yolov5_mnas.getOutputTensor(0);    // output 2 !!!!!!!!
    nnie::Mat output1 = yolov5_mnas.getOutputTensor(1);
    nnie::Mat output2 = yolov5_mnas.getOutputTensor(2);    // output 0 !!!!!!!!

    int img_width = orignImg.cols;
    int img_height = orignImg.rows;
    int num_classes = 5;
    int kBoxPerCell = 3;

    int feature_index0 = 0;
    int feature_index1 = 1;
    int feature_index2 = 2;

    float conf_threshold = 0.15;
    float nms_threshold = 0.45;
    int is_nms = 1;
    int print_level = 1;

    std::vector<int> ids;
    std::vector<cv::Rect> boxes;
    std::vector<float> confidences;

    const std::vector<std::vector<cv::Size2f>> anchors ={
        {{116.0f, 90.0f}, {156.0f, 198.0f}, {373.0f, 326.0f}},
        {{30.0f, 61.0f}, {62.0f, 45.0f}, {59.0f, 119.0f}},
        {{10.0f, 13.0f}, {16.0f, 30.0f}, {33.0f, 23.0f}}};

    // printf("img_h:%d  img_w:%d\n", img_height, img_width);
    parseYolov5Feature(dealImg.cols,dealImg.rows,num_classes,kBoxPerCell,feature_index0,conf_threshold,
                    anchors[0],output0,ids,boxes,confidences, print_level);

    parseYolov5Feature(dealImg.cols,dealImg.rows,num_classes,kBoxPerCell,feature_index1,conf_threshold,
                    anchors[1],output1,ids,boxes,confidences, print_level);

    parseYolov5Feature(dealImg.cols,dealImg.rows,num_classes,kBoxPerCell,feature_index2,conf_threshold,
                    anchors[2],output2,ids,boxes,confidences, print_level);

    std::vector<int> indices;

    char * cls_names[] = {"bkground", "person", "car", "motorcycle", "bus", "truck"};

    if (is_nms)
    {
        cv::dnn::NMSBoxes(boxes, confidences, conf_threshold, nms_threshold, indices);
    } else
    {
        for (int i = 0; i < boxes.size(); ++i)
        {
            indices.push_back(i);
        }
    }

    for (size_t i = 0; i < indices.size(); ++i){
        int idx = indices[i];
        cv::Rect box = boxes[idx];

        // remap box in src input size.
        auto remap_box = RemapBoxOnSrc(cv::Rect2d(box), img_width, img_height);
        nnie::objInfo object_detection;
        object_detection.box = remap_box;
        object_detection.cls_id = ids[idx] + 1;
        object_detection.confidence = confidences[idx];
        detection_results.push_back(std::move(object_detection));
    }
}


void run(){
    const char *model_path = "model.wk";
    nnie::Net yolov5_mnas;
    yolov5_mnas.load_model(model_path);
    cv::Mat orignImg = cv::imread("1.jpg");
    cv::Mat resizedImg;
    cv::resize(orignImg, resizedImg, cv::Size(608, 352));
    std::vector<nnie::objInfo> res;
    v5Infer(yolov5_mnas, resizedImg, res);
    for (auto object_detection : res){
        float xmin = object_detection.box.x;
        float ymin = object_detection.box.y;
        float xmax = object_detection.box.x + object_detection.box.width;
        float ymax = object_detection.box.y + object_detection.box.height;
        float confidence = object_detection.confidence;
        int cls_id = object_detection.cls_id;
        cv::Point p1(xmin, ymin);
        cv::Point p2(xmax, ymax);
        cv::Scalar color(0, 0, 255);
        cv::rectangle(resizedImg, p1, p2, cv::Scalar(200, 0, 0), 1);
    }
    cv::imwrite("out.jpg", resizedImg);
}


void run_car(){
    int detect_num = 5;
    const char *model_path = "model.wk";
    nnie::Net yolov5_mnas;
    yolov5_mnas.load_model(model_path);
    cv::Mat orignImg = cv::imread("1.jpg");
    cv::Mat resizedImg;
    cv::resize(orignImg, resizedImg, cv::Size(608, 352));
    std::vector<nnie::objInfo> res;
    v5Infer(yolov5_mnas, resizedImg, res);
    detect_num = res.size();
    NNIE_Obj_S nnie_res[detect_num];
    int index = 0;
    for (auto object_detection : res){
        float xmin = object_detection.box.x;
        float ymin = object_detection.box.y;
        float xmax = object_detection.box.x + object_detection.box.width;
        float ymax = object_detection.box.y + object_detection.box.height;
        float confidence = object_detection.confidence;
        int cls_id = object_detection.cls_id;
        nnie_res[index].u16X = (MS_U16)xmin;
        nnie_res[index].u16Y = (MS_U16)ymin;
        nnie_res[index].u16W = (MS_U16)(xmax - xmin);
        nnie_res[index].u16H = (MS_U16)(ymax - ymin);
        nnie_res[index].fClasScore = (MS_FLOAT)confidence;
        index++;
        cv::Point p1(xmin, ymin);
        cv::Point p2(xmax, ymax);
        cv::Scalar color(0, 0, 255);
        cv::rectangle(resizedImg, p1, p2, cv::Scalar(200, 0, 0), 1);
    }
    cv::imwrite("out.jpg", resizedImg);

    MS_PARKING_RESULT result;
    MS_VOID* handdle;
    MS_PARKING_CFG_S cfg;
    cfg.detectSens = 5;
    cfg.occupancySens = 5;
    MS_BOOL initRes = MS_Parking_Init(&cfg);
    if (initRes == MS_FALSE){
        // printf("false init\n");
        return;
    }
    for (int i=0; i < 15; i++){
        if(PRINT_DEBUG){
            printf("\n\n=================== round %d  =========================\n", i);
        }
        
        MS_Parking_Get(handdle, nnie_res, (MS_S32)detect_num, &result); 
    }

    for (int i=15; i < 30; i++){
        if(PRINT_DEBUG){
            printf("\n\n=================== round %d  =========================\n", i);
        }
        MS_Parking_Get(handdle, nnie_res, (MS_S32)0, &result); 
    }
    MS_Parking_DeInit(handdle);
}


void run_gen_txt(){
    const char *test_root = "./data/tst_root";
    const char *model_path = "./vca5m2_608x352.wk";
    nnie::Net yolov5_mnas;

    printf("->start to load model\n");
    yolov5_mnas.load_model(model_path);

    printf("->start to get img dirs\n");
    std::vector<std::string> dir_names;
    getAllFiles(test_root, dir_names);
    for(auto image_dir : dir_names){
        yolov5ZrmDemoGenTxt(yolov5_mnas, image_dir);
    }
    
    // for(auto image_dir : dir_names){
    //     std::cout<<image_dir<<std::endl;
    //     std::vector<std::string> file_names;
    //     getAllFiles(image_dir.c_str(), file_names);
    //     yolov5ZrmDemoGenTxt(yolov5_mnas, file_names);
    // }
}

/******************************************************************************
* function 
******************************************************************************/
int main(int argc, char *argv[])
{
    std::cout<<"start"<<std::endl;

    if (argc < 2)
    {
        std::cout<<"param not enough"<<std::endl;
        Usage(argv[0]);
        return -1;
    }
    switch (*argv[1])
    {
        case '0':
        {
            printf("-------yolov5 dir--------\n");
//            Usage(argv[0]);
            const char *image_dir = "./data/input_imgs";
            std::vector<std::string> file_names;
            getAllFiles(image_dir, file_names);
            // printf("finish mat2bgr\n");
            const char *model_path = "./vca5s3_960x544.wk";
            yolov5ZrmDemo(model_path, file_names);
        }
            break;
        case '1':
        {
            run_gen_txt();
        }
            break;

        case 'r':{
            run();
        }
            break;

        case 'c':{
            run_car();
        }
            break;

        default:
        {
            printf("--------default----------\n");
            Usage(argv[0]);
        }
        break;
    }

    return 1;
}


void getAllFiles(const char *path, std::vector<std::string> &files)
{
    DIR *dir;
    struct dirent *ptr;
    if ((dir = opendir(path)) == NULL)
    {
        perror("Open dir error...");
        exit(1);
    }
    while ((ptr = readdir(dir)) != NULL)
    {
        if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0)
            continue;
        else if (ptr->d_type == 8 || ptr->d_type == 0) //file
        {
            // printf("append %s\n",ptr->d_name);
            std::string full_str = "";
            full_str += path;
            full_str += "/";
            full_str += ptr->d_name;
            // printf("append %s\n",full_str.c_str());
            files.push_back(full_str);
        }
    }
    sort(files.begin(), files.end());
    closedir(dir);
}

void Mat2bgr(const cv::Mat im, unsigned char *data)
{
    int h = im.rows;
    int w = im.cols;
    int c = im.channels();
    int step = im.step;
    unsigned char *data1 = (unsigned char *) im.data;
    int count = 0;
    for (int k = 0; k < c; k++)
    {
        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                data[count++] = data1[i * step + j * c + k];
            }
        }
    }
}