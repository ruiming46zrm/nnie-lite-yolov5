#ifndef __MS_TYPE_H__
#define __MS_TYPE_H__

#include <pthread.h>


typedef unsigned char           MS_U8;
typedef unsigned short          MS_U16;
typedef unsigned int            MS_U32;
typedef unsigned long           MS_UL;

typedef char             		MS_CHAR;
typedef signed char             MS_S8;
typedef short           	 	MS_S16;
typedef int                     MS_S32;

typedef float                   MS_FLOAT;
typedef double                  MS_DOUBLE;

#ifndef _M_IX86
    typedef unsigned long long  MS_U64;
    typedef long long           MS_S64;
#else
    typedef __int64             MS_U64;
    typedef __int64             MS_S64;
#endif

typedef char                    MS_CHAR;
typedef void					MS_VOID;

typedef pthread_cond_t          MS_COND_T;
typedef pthread_mutex_t         MS_MUTEX_T;
typedef pthread_t				MS_PTHREAD_T;

#define MS_SUCCESS  0
#define MS_FAILURE  (-1)


/*----------------------------------------------*
 * const defination                             *
 *----------------------------------------------*/
typedef enum {
    MS_FALSE = 0,
    MS_TRUE  = 1,
    MS_NO    = 0,
    MS_YES   = 1,
} MS_BOOL;


#ifndef MS_ABS
#define MS_ABS(a)	   (((a) < 0) ? -(a) : (a))
#endif
#ifndef MS_NZERO
#define MS_NZERO(a)     ((a == 0) ? 1:a)
#endif
#ifndef MS_MAX
#define MS_MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef MS_MIN
#define MS_MIN(a, b)  (((a) < (b)) ? (a) : (b))
#endif


#endif /* __MS_TYPE_H__ */

