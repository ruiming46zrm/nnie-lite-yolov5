#ifndef UTIL_H
#define UTIL_H

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/core.hpp"
#include "Net.hpp"

#ifdef __cplusplus
extern "C"
{
#endif


inline
int getFileLength(const char *file_path)
{
    FILE *FILE;
    int u32FileLen;
    int s32Ret;

    FILE = fopen(file_path, "rb");

    if (NULL != FILE)
    {
        s32Ret = fseek(FILE, 0L, SEEK_END);
        if (0 != s32Ret)
        {
            printf("fseek err!\n");
            fclose(FILE);
            return 0;
        }

        u32FileLen = ftell(FILE);

        s32Ret = fseek(FILE, 0L, SEEK_SET);
        if (0 != s32Ret)
        {
            printf("fseek err!\n");
            fclose(FILE);
            return 0;
        }

        fclose(FILE);
    } else
    {
        printf("open file %s fail!\n", file_path);
        u32FileLen = 0;
    }

    return u32FileLen;
} ;

inline float Sigmoid(float x)
{
    return 1.0 / (1.0 + exp(-x));
} ;

inline void Softmax(std::vector<float> &classes)
{
    float sum = 0;
    std::transform(classes.begin(), classes.end(), classes.begin(),
                   [&sum](float score) -> float
                   {
                       float exp_score = exp(score);
                       sum += exp_score;
                       return exp_score;
                   });
    std::transform(classes.begin(), classes.end(), classes.begin(),
                   [sum](float score) -> float
                   { return score / sum; });
} ;

inline void parseYolov5Feature(int img_width,
                               int img_height,
                               int num_classes,
                               int kBoxPerCell,
                               int feature_index,
                               float conf_threshold,
                               const std::vector<cv::Size2f> &anchors,
                               const nnie::Mat feature,
                               std::vector<int> &ids,
                               std::vector<cv::Rect> &boxes,
                               std::vector<float> &confidences,
                               int print_level)
{

    // if (feature_index == 0){
    //     feature_index = 2;
    // } else if (feature_index == 2){
    //     feature_index = 0;
    // }

    const float downscale = static_cast<float>(std::pow(2, feature_index) / 32); // downscale, 1/32, 1/16, 1/8

    // int cell_w = (int)std::pow(feature.width, 0.5);
    // int cell_h = cell_w;

    int cell_w = (int)(img_width * downscale);
    int cell_h = (int)(img_height * downscale);
    

    /* 
    -1: not print
     0: basic log
     1: detail log
     2: feature msg 
    */
    if(print_level>=1){
        printf("\n");
        printf("-------parseYolov5Feature------\n");
        printf("img_width:%d, img_height:%d \n num_classes:%d, kBoxPerCell:%d, feature_index:%d \n", img_width, img_height, num_classes, kBoxPerCell, feature_index);
        printf("conf_threshold:%f \n", conf_threshold);
        printf("downscale:%f \n", downscale);
        printf("feature msg: w:%d, h:%d, ch:%d, n:%d\n", feature.width, feature.height, feature.channel, feature.n);
        printf("cell_w:%d,  cell_h:%d\n", cell_w, cell_h);
    }
    

    if(print_level == 2){
        printf("\n\n=================feature info==========================\n");
        for (int cc = 0; cc < feature.channel; cc++){
            for (int cy = 0; cy < feature.height; ++cy){
                for (int cx = 0; cx < feature.width; ++cx){
                    printf("%f ", feature.data[cx + cy * feature.width + cc * feature.width * feature.height]);
                }
                printf("\n\n");
            }
        }
        printf("===========================================\n\n");
    }


    for (int cy = 0; cy < cell_h; ++cy)
    {
        for (int cx = 0; cx < cell_w; ++cx)
        {
            for (int b = 0; b < kBoxPerCell; ++b)
            {
                int channel = b * (num_classes + 5);

                float tc = feature.data[cx + (cy * cell_w) + (channel + 4) * cell_h * cell_w];

                float confidence = Sigmoid(tc);

                if (confidence >= conf_threshold)
                {
                    float tx = feature.data[cx + (cy * cell_w) + channel * cell_h * cell_w];
                    float ty = feature.data[cx + (cy * cell_w) + (channel + 1) * cell_h * cell_w];
                    float tw = feature.data[cx + (cy * cell_w) + (channel + 2) * cell_h * cell_w];
                    float th = feature.data[cx + (cy * cell_w) + (channel + 3) * cell_h * cell_w];
                    float tc = feature.data[cx + (cy * cell_w) + (channel + 4) * cell_h * cell_w];

                    tx = Sigmoid(tx);
                    ty = Sigmoid(ty);
                    tw = Sigmoid(tw);
                    th = Sigmoid(th);

                    float x = ((float)cx - 0.5f + 2.0f * tx) / cell_w;
                    float y = ((float)cy - 0.5f + 2.0f * ty) / cell_h;
                    float w = (2.0f *  tw) * (2.0f *  tw) * anchors[b].width * downscale / cell_w;
                    float h = (2.0f * th) * (2.0f * th) * anchors[b].height * downscale / cell_h;
                    
                    // // CV_LOG_INFO(NULL, "cx:" << cx << " cy:" << cy);
                    // // CV_LOG_INFO(NULL, "box:" << x << " " << y << " " << w << " " << h << " c:" << confidence
                    // //                          << " index:" << index);
                    std::vector<float> classes(num_classes);
                    for (int i = 0; i < num_classes; ++i)
                    {
                        float tc_by_class = feature.data[cx + (cy * cell_w) + (channel + 5 + i) * cell_h * cell_w];
                        float tc_by_class_sigmoid = Sigmoid(tc_by_class);
                        classes[i] = tc_by_class_sigmoid;
                    }
                    // Softmax(classes);
                    auto max_itr = std::max_element(classes.begin(), classes.end());
                    int index = static_cast<int>(max_itr - classes.begin());
                    if (num_classes > 1){
                        confidence = confidence * classes[index];
                    }
                    int center_x = (int) (x * img_width);
                    int center_y = (int) (y * img_height);
                    int width = (int) (w * img_width);
                    int height = (int) (h * img_height);
                    int left = static_cast<int>(center_x - (width - 1.0f) * 0.5f);
                    int top = static_cast<int>(center_y - (height - 1.0f) * 0.5f);

                    // // CV_LOG_INFO(NULL,
                    // //             "box:" << left << " " << top << " " << width << " " << height << " c:" << confidence
                    // //                    << " index:" << index
                    // //                    << " p:" << classes[index]);
                    if (confidence > conf_threshold){
                        ids.push_back(index);
                        boxes.emplace_back(left, top, width, height);
                        confidences.push_back(confidence);
                    }
                }
            }
        }
    }

    if(print_level==2){
        printf("\n------boxes info------\n");
        int print_num = 0;
        for(int print_idx=0; print_idx < boxes.size(); print_idx++){
            float mleft = boxes[print_idx].x;
            float mtop = boxes[print_idx].y;
            float mw = boxes[print_idx].width;
            float mh = boxes[print_idx].height;
            float mscore = (float)confidences[print_idx];
            int midx = ids[print_idx];

            print_num++;
            printf("left:%f  top:%f  w:%f  h:%f  class:%d  score:%f\n", mleft, mtop, mw, mh, midx, mscore);
        }
        printf("total num: %d\n", print_num);
    }
} ;


inline void parseYolov3Feature(int img_width,
                               int img_height,
                               int num_classes,
                               int kBoxPerCell,
                               int feature_index,
                               float conf_threshold,
                               const std::vector<cv::Size2f> &anchors,
                               const nnie::Mat feature,
                               std::vector<int> &ids,
                               std::vector<cv::Rect> &boxes,
                               std::vector<float> &confidences)
{
    const float downscale = static_cast<float>(std::pow(2, feature_index) / 32); // downscale, 1/32, 1/16, 1/8
    int cell_h = feature.height;
    int cell_w = feature.width;

    printf("\n");
    printf("-------parseYolov3Feature------\n");
    printf("img_width:%d, img_height:%d \n num_classes:%d, kBoxPerCell:%d, feature_index:%d \n", img_width, img_height, num_classes, kBoxPerCell, feature_index);
    printf("conf_threshold:%f \n", conf_threshold);
    printf("downscale:%f \n", downscale);
    printf("feature msg: w:%d, h:%d, ch:%d, n:%d\n", feature.width, feature.height, feature.channel, feature.n);



    for (int cy = 0; cy < cell_h; ++cy)
    {
        for (int cx = 0; cx < cell_w; ++cx)
        {
            for (int b = 0; b < kBoxPerCell; ++b)
            {
                int channel = b * (num_classes + 5);

                float tc = feature.data[cx + (cy * feature.width) + (channel + 4) * feature.height * feature.width];

                float confidence = Sigmoid(tc);

                if (confidence >= conf_threshold)
                {

                    float tx = feature.data[cx + (cy * feature.width) + channel * feature.height * feature.width];
                    float ty = feature.data[cx + (cy * feature.width) + (channel + 1) * feature.height * feature.width];
                    float tw = feature.data[cx + (cy * feature.width) + (channel + 2) * feature.height * feature.width];
                    float th = feature.data[cx + (cy * feature.width) + (channel + 3) * feature.height * feature.width];
                    float tc = feature.data[cx + (cy * feature.width) + (channel + 4) * feature.height * feature.width];

                    float x = (cx + Sigmoid(tx)) / cell_w;
                    float y = (cy + Sigmoid(ty)) / cell_h;
                    float w = exp(tw) * anchors[b].width * downscale / cell_w;
                    float h = exp(th) * anchors[b].height * downscale / cell_h;
                    // CV_LOG_INFO(NULL, "cx:" << cx << " cy:" << cy);
                    // CV_LOG_INFO(NULL, "box:" << x << " " << y << " " << w << " " << h << " c:" << confidence
                    //                          << " index:" << index);
                    std::vector<float> classes(num_classes);
                    for (int i = 0; i < num_classes; ++i)
                    {
                        classes[i] = feature.data[cx + (cy * feature.width) +
                                                  (channel + 5 + i) * feature.height * feature.width];
                    }
                    Softmax(classes);
                    auto max_itr = std::max_element(classes.begin(), classes.end());
                    int index = static_cast<int>(max_itr - classes.begin());
                    if (num_classes > 1)
                    {
                        confidence = confidence * classes[index];
                    }

                    int center_x = (int) (x * img_width);
                    int center_y = (int) (y * img_height);
                    int width = (int) (w * img_width);
                    int height = (int) (h * img_height);
                    int left = static_cast<int>(center_x - width / 2);
                    int top = static_cast<int>(center_y - height / 2);
                    // CV_LOG_INFO(NULL,
                    //             "box:" << left << " " << top << " " << width << " " << height << " c:" << confidence
                    //                    << " index:" << index
                    //                    << " p:" << classes[index]);

                    ids.push_back(index);
                    boxes.emplace_back(left, top, width, height);
                    confidences.push_back(confidence);
                }
            }
        }
    }
} ;


inline void parseYolov2Feature(int img_width,
                               int img_height,
                               int num_classes,
                               int kBoxPerCell,
                               int feature_index,
                               float conf_threshold,
                               const std::vector<cv::Size2f> &anchors,
                               const nnie::Mat feature,
                               std::vector<int> &ids,
                               std::vector<cv::Rect> &boxes,
                               std::vector<float> &confidences)
{
    const float downscale = static_cast<float>(std::pow(2, feature_index) / 32); // downscale, 1/32, 1/16, 1/8
    int cell_h = feature.height;
    int cell_w = feature.width;
    for (int cy = 0; cy < cell_h; ++cy)
    {
        for (int cx = 0; cx < cell_w; ++cx)
        {
            for (int b = 0; b < kBoxPerCell; ++b)
            {
                int channel = b * (num_classes + 5);

                float tc = feature.data[cx + (cy * feature.width) + (channel + 4) * feature.height * feature.width];

                float confidence = Sigmoid(tc);

                if (confidence >= conf_threshold)
                {

                    float tx = feature.data[cx + (cy * feature.width) + channel * feature.height * feature.width];
                    float ty = feature.data[cx + (cy * feature.width) + (channel + 1) * feature.height * feature.width];
                    float tw = feature.data[cx + (cy * feature.width) + (channel + 2) * feature.height * feature.width];
                    float th = feature.data[cx + (cy * feature.width) + (channel + 3) * feature.height * feature.width];
                    float tc = feature.data[cx + (cy * feature.width) + (channel + 4) * feature.height * feature.width];

                    float x = (cx + Sigmoid(tx)) / cell_w;
                    float y = (cy + Sigmoid(ty)) / cell_h;
                    float w = exp(tw) * anchors[b].width / cell_w;
                    float h = exp(th) * anchors[b].height / cell_h;
                    // CV_LOG_INFO(NULL, "cx:" << cx << " cy:" << cy);
                    // CV_LOG_INFO(NULL, "box:" << x << " " << y << " " << w << " " << h << " c:" << confidence
                    //                          << " index:" << index);
                    std::vector<float> classes(num_classes);
                    for (int i = 0; i < num_classes; ++i)
                    {
                        classes[i] = feature.data[cx + (cy * feature.width) +
                                                  (channel + 5 + i) * feature.height * feature.width];
                    }
                    Softmax(classes);
                    auto max_itr = std::max_element(classes.begin(), classes.end());
                    int index = static_cast<int>(max_itr - classes.begin());
                    if (num_classes > 1)
                    {
                        confidence = confidence * classes[index];
                    }

                    int center_x = (int) (x * img_width);
                    int center_y = (int) (y * img_height);
                    int width = (int) (w * img_width);
                    int height = (int) (h * img_height);
                    int left = static_cast<int>(center_x - width / 2);
                    int top = static_cast<int>(center_y - height / 2);
//                         CV_LOG_INFO(NULL,
//                                     "box:" << left << " " << top << " " << width << " " << height << " c:" << confidence
//                                            << " index:" << index
//                                            << " p:" << classes[index]);

                    ids.push_back(index);
                    boxes.emplace_back(left, top, width, height);
                    confidences.push_back(confidence);
                }
            }
        }
    }
} ;

inline cv::Rect2d RemapBoxOnSrc(const cv::Rect2d &box, const int img_width, const int img_height)
{
    float xmin = static_cast<float>(box.x);
    float ymin = static_cast<float>(box.y);
    float xmax = xmin + static_cast<float>(box.width);
    float ymax = ymin + static_cast<float>(box.height);
    cv::Rect2d remap_box;
    remap_box.x = std::max(.0f, xmin);
    remap_box.width = std::min(img_width - 1.0f, xmax) - remap_box.x;
    remap_box.y = std::max(.0f, ymin);
    remap_box.height = std::min(img_height - 1.0f, ymax) - remap_box.y;
    return remap_box;
} ;

void LetterBox(cv::Mat &src_img, cv::Mat &dst_img, int &dst_w_o, int &dst_h_o, float &r, float &dw, float &dh){
    float dst_w = (float) dst_w_o;
    float dst_h = (float) dst_h_o;
    float src_w = (float)src_img.cols;
    float src_h = (float)src_img.rows;
    r = std::min(dst_h/src_h, dst_w/src_w);
    int new_unpad_w = (int) (round(src_w * r));
    int new_unpad_h = (int) (round(src_h * r));
    dw = 0.5 * (dst_w - new_unpad_w);
    dh = 0.5 * (dst_h - new_unpad_h);
    cv::Mat new_unpad_img;
    cv::resize(src_img, new_unpad_img, cv::Size(new_unpad_w, new_unpad_h));
    int top = (int)(round(dh - 0.1));
    int bottom = (int) (round(dh + 0.1));
    int left = (int) (round(dw - 0.1));
    int right = (int) (round(dw + 0.1));
    std::cout<<"dw:"<<dw<<"  dh:"<<dh<<std::endl;
    cv::copyMakeBorder(new_unpad_img, dst_img, top, bottom, left, right, cv::BORDER_CONSTANT, cv::Scalar::all(0));
    std::cout<<"img_w:"<<dst_img.cols<<"  img_h:"<<dst_img.rows<<std::endl;
}

inline cv::Rect2d RemapBoxOnSrcLetterBox(const cv::Rect2d &box, const int img_width, const int img_height, float r, float dw, float dh)
{
    float xmin = static_cast<float>(box.x);
    float ymin = static_cast<float>(box.y);
    float xmax = xmin + static_cast<float>(box.width);
    float ymax = ymin + static_cast<float>(box.height);

    xmin -= dw;
    xmax -= dw;
    ymin -= dh;
    ymax -= dh;
    
    xmin /= r;
    xmax /= r;
    ymin /= r;
    ymax /= r;

    cv::Rect2d remap_box;
    remap_box.x = std::max(.0f, xmin);
    remap_box.width = std::min(img_width - 1.0f, xmax) - remap_box.x;
    remap_box.y = std::max(.0f, ymin);
    remap_box.height = std::min(img_height - 1.0f, ymax) - remap_box.y;
    return remap_box;
} ;

#ifdef __cplusplus
}
#endif
#endif