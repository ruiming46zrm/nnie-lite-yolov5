// #include <> 
#pragma once
#include "mstype.h"

#define MS_MAX(a,b)    (((a) > (b)) ? (a) : (b))
#define MS_MIN(a,b)    (((a) < (b)) ? (a) : (b))

#define PARKING_SPACE_MAX 100
#define PARKING_JUDGE_FRAME_MAX 20  
#define DETECT_SENS_MAX 10 
#define OCCUPANCY_SENS_MAX 10  

#define PRINT_DEBUG 0

typedef struct {
    MS_U16 u16X;
    MS_U16 u16Y;
    MS_U16 u16W;
    MS_U16 u16H;
    // NetworkModelType_E modelType;
    MS_FLOAT fClasScore;
} NNIE_Obj_S;

typedef struct PKPoint {
    MS_S32 s32X;
    MS_S32 s32Y;
} PK_POINT_S;

typedef struct Quadrangle {
    PK_POINT_S vertexs[4]; 
} QUADRANGLE_S; 

typedef struct ParkingSpace {
	MS_S32 id;
	MS_BOOL occupancy; // 是否占用 MS_TRUE OR MS_FALSE
	QUADRANGLE_S location; // 车位位置
	MS_U64 startTime; // 占用的起始时间
	MS_U64 endTime; // 占用的结束时间
	MS_U32 judgeParkingLst[PARKING_JUDGE_FRAME_MAX];
} PARKING_SPACE_S;

typedef struct MSParkingCfg {
	MS_S32 detectSens; // 检测 灵敏度阈值
	MS_S32 occupancySens;  // 占用灵敏度阈值 0-100
	PARKING_SPACE_S parkingSpaces[PARKING_SPACE_MAX]; // 预设的车位
	MS_S32 spaceNum; // 预设的车位数量
}MS_PARKING_CFG_S; 

// 占用时间
// 20个区域
// 根据handdle来作为一个执行对象

typedef struct MSParkingResult {
	NNIE_Obj_S* alarmObj; // 报警目标————触发占用的目标
	MS_S32 objNum; 
	PARKING_SPACE_S* alarmSpace; // 已占用的车位
	MS_S32 SpaceNum; 
}MS_PARKING_RESULT; 

MS_BOOL MS_Parking_Init(MS_PARKING_CFG_S* cfg); 
MS_VOID MS_Parking_DeInit(MS_VOID* handdle); 
MS_VOID MS_Parking_Get(MS_VOID* handdle, NNIE_Obj_S* objList, MS_S32 num, MS_PARKING_RESULT* result); 