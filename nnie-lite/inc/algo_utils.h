#pragma once
#include "mstype.h"
#include<iostream>
#include<algorithm>
#include<cmath>
#include <vector>
#include <cstring>
#include <cstdio>

MS_DOUBLE iou_poly(MS_DOUBLE* p, MS_DOUBLE* q);

MS_S32 KM(MS_FLOAT* lv, MS_S32 car_num, MS_S32 space_num, MS_S32* car_match, MS_S32* space_match);